
provider "aws" {
  region = "eu-west-3"
}

variable vpc_cidr_block {}
variable public_subnet_cidr_block {}
variable private_subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}
variable my_ip {}
variable instance_type {}
variable instance_type_jenkins {}
variable ssh_key_private {}
variable username {}
variable ubuntu_username {}
variable ansible_dir {}
variable ansible_ip {}
variable ansible_p_ip {}

resource "aws_vpc" "ansible-vpc" {
    cidr_block = var.vpc_cidr_block
    enable_dns_hostnames = true
    
    tags = {
      Name = "ans-vpc"
    }
}

// Private Subnet
resource "aws_subnet" "ans-private-subnet" {
  vpc_id = aws_vpc.ansible-vpc.id
  cidr_block = var.private_subnet_cidr_block
  availability_zone = var.avail_zone
  tags = {
      Name = "ans-private-subnet-1"
    }
}

// Public Subnet
resource "aws_subnet" "ans-public-subnet" {
  vpc_id = aws_vpc.ansible-vpc.id
  cidr_block = var.public_subnet_cidr_block
  availability_zone = var.avail_zone
  tags = {
      Name = "ans-public-subnet-1"
    }
}

resource "aws_internet_gateway" "ansible-igw" {
  vpc_id = aws_vpc.ansible-vpc.id

  tags = {
    Name = "ansible-igw"
  }
}

resource "aws_eip" "ans-eip" {
  domain   = "vpc"

  depends_on = [aws_internet_gateway.ansible-igw]
}

resource "aws_nat_gateway" "ans-nat_gateway" {
  allocation_id = aws_eip.ans-eip.id
  subnet_id     = aws_subnet.ans-public-subnet.id

  tags = {
    Name = "my-nat"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.ansible-igw]
}

resource "aws_route_table" "myapp-rt" {
  vpc_id = aws_vpc.ansible-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ansible-igw.id
  }

  tags = {
    Name = "myapp-rt"
  }
}

// Route table for the NAT Gateway
resource "aws_route_table" "my-db-rt" {
  vpc_id = aws_vpc.ansible-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.ans-nat_gateway.id
  }

  tags = {
    Name = "my-db-rt"
  }
}

resource "aws_route_table_association" "a-rtb-public-subnet" {
  subnet_id = aws_subnet.ans-public-subnet.id
  route_table_id = aws_route_table.myapp-rt.id
}

resource "aws_route_table_association" "a-rtb-private-subnet" {
  subnet_id = aws_subnet.ans-private-subnet.id
  route_table_id = aws_route_table.my-db-rt.id
}

resource "aws_security_group" "ansible-sg" {
  name = "ansible-sg"
  vpc_id = aws_vpc.ansible-vpc.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.ansible_ip]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.ansible_p_ip]
  }

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "ansible-sg"
  }

}

resource "aws_security_group" "db-sg" {
  name = "db-sg"
  vpc_id = aws_vpc.ansible-vpc.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.ansible_ip]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.ansible_p_ip]
  }

  ingress {
    from_port           = 3306
    to_port             = 3306
    protocol            = "tcp"
    security_groups     = [aws_security_group.ansible-sg.id] # Reference the web server's security group
  }

/*
  ingress {
    from_port           = 3306
    to_port             = 3306
    protocol            = "tcp"
    cidr_blocks = ["13.36.230.102/32"] # webserver public ip
  }

  ingress {
    from_port           = 3306
    to_port             = 3306
    protocol            = "tcp"
    cidr_blocks = ["10.0.10.132/32"] # webserver private ip
  }
*/

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "db-sg"
  }

}

/*
# LINUX 2023
data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name = "name"
    values = ["al2023-ami-*-x86_64"]
  }
}
*/
# LINUX 2
/*
data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name = "name"
    values = ["amzn2-ami-*-x86_64-gp2"]
  }
}
*/

# UBUNTU
/*
data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners = ["099720109477"]

  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}
*/

// APP INSTANCE
/*
resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type

  subnet_id = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [ aws_security_group.myapp-sg.id ]
  availability_zone = var.avail_zone

  associate_public_ip_address = true
  key_name = "myapp-key-pair"

  tags = {
    Name = "${var.env_prefix}-server"
  }

}
*/
/*
// JENKINS INSTANCE
resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type_jenkins

  subnet_id = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [ aws_security_group.myapp-sg.id ]
  availability_zone = var.avail_zone

  associate_public_ip_address = true
  key_name = "myapp-key-pair"

  # root_block_device {
  #   volume_size = 6 # in GBs
  #   volume_type = "gp2"
  #   encrypted   = true
  # }

  tags = {
    Name = "jenkins-server"
  }

}

resource "null_resource" "configure_server" {
  triggers = {
    trigger = aws_instance.myapp-server.public_ip
  }

}
*/

# resource "aws_ebs_volume" "st1" {
#   availability_zone = aws_instance.myapp-server.availability_zone
#   size = 6   // In GBs
#   tags= {
#     Name = "jenkins-volume"
#   }
# }

# resource "aws_volume_attachment" "ebs" {
#   device_name = "/dev/xvda"
#   volume_id = aws_ebs_volume.st1.id
#   instance_id = aws_instance.myapp-server.id
# }
