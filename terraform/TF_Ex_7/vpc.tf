provider "aws" {
    region = var.avail_zone
}

variable vpc_cidr_block {}
variable private_subnet_cidr_blocks {}
variable public_subnet_cidr_blocks {}
variable avail_zone {}

# This will get the availability zones from the region specified in the provider
data "aws_availability_zones" "azs" {}

module "myapp-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.0.0"

  name = "myapp-vpc"
  cidr = var.vpc_cidr_block

  azs = data.aws_availability_zones.azs.names
  private_subnets = var.private_subnet_cidr_blocks
  public_subnets  = var.public_subnet_cidr_blocks
  
  enable_nat_gateway = true

  # The private subnets will be able to route traffic through this nat gateway
  single_nat_gateway = true

  # When the EC2s are created they will get their private and public ips. 
  # With this conf. they will also get the dns hostnames that will resolve to those public ips.
  enable_dns_hostnames = true

  # These tags are necessary. They are used by Kubernetes to associate the subnets to this specific cluster
  tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
  }

  # These tags are necessary. They are used by Kubernetes to associate the subnets to the proper elastic load balancer
  public_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/elb" = 1
  }

  # These tags are necessary. They are used by Kubernetes to associate the subnets to the proper elastic load balancer
  private_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/internal-elb" = 1
  }

}