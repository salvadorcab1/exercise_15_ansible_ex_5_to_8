variable cluster_name {}
variable cluster_version {}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.15.3"

  cluster_name    = var.cluster_name
  cluster_version = var.cluster_version

  # We require just the private subnet ids where the workload will be scheduled in the project
  subnet_ids = module.myapp-vpc.private_subnets

  # vpc id
  vpc_id = module.myapp-vpc.vpc_id

  # Per default the cluster sends the endpoint to the private access. That's why we have to add these lines
  cluster_endpoint_private_access = false
  cluster_endpoint_public_access = true

  eks_managed_node_groups = {
    # The nodegroup name will be dev
    dev = {
      min_size     = 1
      max_size     = 3
      desired_size = 3

      instance_types = ["t2.small"]
    }
  }

  tags = {
    environment = "development"
    application = "myapp"
  }

}